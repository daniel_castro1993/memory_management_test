#ifndef RANGE_H_GUARD_
#define RANGE_H_GUARD_

#include <stdint.h>

// addr has the missing location
typedef void(*range_before_swap_in)(void *addr, void *metadata, void *args);

// indicate the address to swap-out in *addr, return 1 to swap out another page
// return NULL in *addr to swap-out any page (default policy)
typedef int(*range_before_swap_out)(void **addr, void *metadata, void *args);

typedef struct range_top_node_* range_top_node_s;

void range_init(uint64_t dram_buffer_size);
void range_thread_enter();
int64_t range_thread_get_id();
void range_thread_exit();
void range_destroy();

void range_add(
    const char *filepath,
    void *base,
    uint64_t size/* must be multiple of page_size */,
    int page_size/* must be power 2 */,
    int metadata_size,
    int is_private,
    int is_trigger_CoW,
    range_before_swap_in on_swap_in,
    range_before_swap_out on_swap_out,
    void *args);
void range_rm(void *base, uint64_t size);

// internal
void range_add_page(
    void *base,
    uint64_t range_size,
    void *addr,
    uint64_t page_size,
    uint64_t page_mask,
    uint64_t page_bits,
    int fd,
    int is_private,
    int is_trigger_CoW
);
void range_rm_page(
    void *base,
    uint64_t range_size,
    void *addr,
    uint64_t page_size,
    uint64_t page_mask,
    uint64_t page_bits
);

#endif /* RANGE_H_GUARD_ */