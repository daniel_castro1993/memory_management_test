#ifndef REPL_H_GUARD
#define REPL_H_GUARD

#include <stdint.h>

typedef struct repl_ *repl_s;
typedef struct repl_thrd_ *repl_thrd_s;
typedef struct repl_range_ *repl_range_s;

repl_s repl_init(
    const char *logs_file,
    int nb_thrds,
    uint64_t per_thrd_log_size
);
void repl_logs_erase(repl_s);
repl_thrd_s repl_thread_enter(repl_s);
void repl_thread_exit(repl_thrd_s);
int repl_thread_get_id(repl_thrd_s r);
void repl_destroy(repl_s);
void repl_set_workers_done(repl_s r, int done);

// TODO: not needed for PM
void repl_flush_mmap(repl_s r);

// mmaps the range in shared mode
repl_range_s repl_mmap_range(
    const char *filename,
    void *base, 
    uint64_t range_size
);
void repl_munmap_range(repl_range_s);

uint64_t repl_log_left(int log_id, repl_s r);

// write a chunk with repl_store then end with repl_close
void repl_start(repl_thrd_s);
void repl_store(repl_thrd_s, void *addr, uint64_t value);
void repl_close(repl_thrd_s, uint64_t timestamp);

// TODO: multithreaded, NUMA-aware, linking, dupplicate filter, -.-
void repl_replay_init(repl_s);
int repl_replay(repl_s); // returns whether the workers flagged that it is done
void repl_replay_exit(repl_s);

#endif /* REPL_H_GUARD */