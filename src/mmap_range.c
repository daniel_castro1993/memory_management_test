#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <assert.h>

#include "mmap_range.h"
#include "util.h"

struct range_top_node_
{
    void *metadata;
    uint64_t *is_page_in;
    uint64_t *is_removing_page;
    uint64_t *is_inserting_page;
    int fd;
    int is_private;
    int is_trigger_cow;
    int page_size;
    int metadata_size;
    int total_nb_pages;
    uint64_t page_mask;
    uint64_t page_bits;
    uintptr_t base_addr;
    uint64_t size_range;
    volatile int last_sel_page;
    range_before_swap_in on_swap_in;
    range_before_swap_out on_swap_out;
    void *args;
    struct range_top_node_ *next;
};

static range_top_node_s list_of_ranges = NULL;
static uint64_t dram_buffer_size;
static uint64_t curr_dram_buffer_size;
static volatile int64_t thread_count;
static __thread int64_t thread_id;

static void heap_sigsegv_handler(int sig, siginfo_t *si, void *unused);

void range_init(uint64_t dram_buffer_size_)
{
    struct sigaction sa;

    dram_buffer_size = dram_buffer_size_;
    curr_dram_buffer_size = 0;

    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = heap_sigsegv_handler;
    if (-1 == sigaction(SIGSEGV, &sa, NULL))
        perror("sigaction()");
}

void range_thread_enter()
{
    thread_id = __atomic_fetch_add(&thread_count, 1, __ATOMIC_SEQ_CST);
}

int64_t range_thread_get_id()
{
    return thread_id;
}

void range_thread_exit()
{
    // TODO: cannot decrement the thread_count
}

void range_destroy()
{

}

void range_add(
    const char *filepath,
    void *base,
    uint64_t size/* must be multiple of page_size */,
    int page_size/* must be power 2 */,
    int metadata_size,
    int is_private,
    int is_trigger_CoW,
    range_before_swap_in on_swap_in,
    range_before_swap_out on_swap_out,
    void *args
) {
    uint64_t total_nb_pages = size / page_size;
    range_top_node_s n = list_of_ranges;
    range_top_node_s prev = n;
    range_top_node_s new = (range_top_node_s)calloc(1, sizeof(struct range_top_node_));
    assert(NULL != new && "could not allocate new range");
    new->is_page_in = (uint64_t*)calloc(total_nb_pages >> 6, sizeof(uint64_t));
    assert(NULL != new->is_page_in && "could not allocate page_in bit");
    new->is_removing_page = (uint64_t*)calloc(total_nb_pages >> 6, sizeof(uint64_t));
    assert(NULL != new->is_removing_page && "could not allocate removing_in bit");
    new->is_inserting_page = (uint64_t*)calloc(total_nb_pages >> 6, sizeof(uint64_t));
    assert(NULL != new->is_inserting_page && "could not allocate inserting_page bit");
    new->metadata = calloc(total_nb_pages, metadata_size);
    assert(NULL != new->metadata && "could not allocate metadata");

    uintptr_t addr = (uintptr_t)base;
    while (NULL != n && addr >= n->base_addr)
    {
        prev = n;
        n = n->next;
    }
    if (NULL == prev)
        list_of_ranges = new;
    else
    {
        assert(NULL != n && addr - n->base_addr < n->size_range && "range already mapped");
        prev->next = new;
    }
    new->next = n;
    new->base_addr = addr;
    new->size_range = size;
    new->page_bits = fastlog2_and_mask(page_size, &new->page_mask, NULL);
    new->page_size = 1L << new->page_bits;
    new->total_nb_pages = total_nb_pages;
    new->is_private = is_private;
    new->is_trigger_cow = is_trigger_CoW;
    new->on_swap_in = on_swap_in;
    new->on_swap_out = on_swap_out;
    new->metadata_size = metadata_size;
    new->args = args;

    struct stat heapfile_buffer;
    if (stat(filepath, &heapfile_buffer))
    {
        new->fd = open(filepath, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        close(new->fd); // writes the permissions
        if (-1 == (new->fd = open(filepath, O_RDWR, 0666)))
            fprintf(stderr, "Error open file \"%s\": %s\n", filepath, strerror(errno));
        if (ftruncate(new->fd, size))
            fprintf(stderr, "Error ftruncate file \"%s\": %s\n", filepath, strerror(errno));
    }
    else
    {
        // file exists
        if (-1 == (new->fd = open(filepath, O_RDWR, 0666)))
            fprintf(stderr, "Error open file \"%s\": %s\n", filepath, strerror(errno));
        if (heapfile_buffer.st_size < size)
        {
            if (ftruncate(new->fd, size))
                fprintf(stderr, "Error ftruncate file \"%s\": %s\n", filepath, strerror(errno));
        }
    }
}

void range_rm(void *base, uint64_t size)
{
    range_top_node_s n = list_of_ranges;
    range_top_node_s prev = n;
    uintptr_t addr = (uintptr_t)base;
    while (NULL != n && addr - n->base_addr >= n->size_range)
    {
        prev = n;
        n = n->next;
    }
    assert(NULL == n && "no range mapped");
    if (prev == n)
    {
        list_of_ranges = n->next;
    }
    else
    {
        prev->next = n->next;
    }
// exit:
    close(n->fd);
    free(n->metadata);
    free(n->is_page_in);
    free(n);
}

static void *select_page_out(range_top_node_s *node)
{
    const int max_tries = 1000; // TODO
    int i = 0;
    int tries = 0;
    uint64_t page_in, old_sel;
    range_top_node_s n;
    int tot;

try_here:
    n = list_of_ranges;
    tot = (n->total_nb_pages >> 6);
    while (NULL != n)
    {
        for (i = (old_sel = __atomic_load_n(&n->last_sel_page, __ATOMIC_ACQUIRE)); i < tot; ++i)
        {
            if (0 != (page_in = __atomic_load_n(&n->is_page_in[i], __ATOMIC_ACQUIRE)))
            {
                __atomic_fetch_add(&n->last_sel_page, i - old_sel + 1, __ATOMIC_RELEASE);
                uint64_t p = fastlog2_and_mask(page_in, NULL, NULL);
                uintptr_t offset = 64L*i + p;
                uintptr_t pagetorm = n->base_addr + offset*n->page_size;
                uintptr_t metadata = (uintptr_t)n->metadata;
                void *addr_page_rem = (void*)pagetorm;
                metadata += offset*n->metadata_size;
                // printf("sel %p i=%i nb_pages=%i offset=%lx\n", addr_page_rem, i, n->total_nb_pages, offset);
                *node = n;
                n->on_swap_out(&addr_page_rem, (void*)metadata, n->args);
                return addr_page_rem;
            }
        }
        __atomic_store_n(&n->last_sel_page, 0, __ATOMIC_RELEASE);
        n = n->next;
    }
    if (tries < max_tries)
    {
        tries++;
        goto try_here;
    }
    // TODO: in multithreaded env it could happen that it cannot select a page
    // assert(0 && "could not select a page out");
    return NULL;
}

static void rm_page(range_top_node_s n, void *addr)
{
    uintptr_t toUnmap = (uintptr_t)addr;
    toUnmap &= ~n->page_mask;
    uintptr_t a = toUnmap;
    a -= n->base_addr;
    // printf("munmap %p of size %d a = %p range = %lx\n", (void*)toUnmap, n->page_size, (void*)a, n->size_range);
    assert(a < n->size_range);
    a >>= n->page_bits;
    int loc64    = a & 0x3F;
    int locPage  = a >> 6;
    int success = 0;
    uint64_t old_val;
    uint64_t new_val;
    if (((1L<<loc64)&__atomic_load_n(&n->is_page_in[locPage], __ATOMIC_ACQUIRE)) != (1L<<loc64))
        return; // someone already removed the page
    while (((1L<<loc64)&(old_val=__atomic_load_n(&n->is_removing_page[locPage], __ATOMIC_ACQUIRE))) != (1L<<loc64))
    {  
        new_val = old_val | (1L<<loc64);
        success = __atomic_compare_exchange_n(&n->is_removing_page[locPage], &old_val, new_val, /*strong*/0, __ATOMIC_ACQ_REL, __ATOMIC_ACQUIRE);
    }
    if (!success)
        return; // someone else is removing
#ifndef NDEBUG
    int err =
#endif
    munmap((void*)toUnmap, n->page_size);
    __atomic_and_fetch(&n->is_page_in[locPage], ~(1L<<loc64), __ATOMIC_RELEASE);
    __atomic_sub_fetch(&curr_dram_buffer_size, n->page_size, __ATOMIC_RELEASE);
    __atomic_and_fetch(&n->is_removing_page[locPage], ~(1L<<loc64), __ATOMIC_RELEASE);
    assert(!err && "could not unmap the page");
}

void add_page(range_top_node_s n, void *addr)
{
    uintptr_t toMap = (uintptr_t)addr;
    toMap &= ~n->page_mask;
    uintptr_t a = toMap;
    a -= n->base_addr; // subtracts base addr
    off_t location_in_file = (off_t)a;
    assert(a < n->size_range);
    a >>= n->page_bits;
    int loc64    = a & 0x3F;
    int locPage  = a >> 6; // log2(64) == 6
    int success = 0;
    uint64_t old_val;
    uint64_t new_val;
    if (((1L<<loc64)&__atomic_load_n(&n->is_page_in[locPage], __ATOMIC_ACQUIRE)) == (1L<<loc64))
        return; // someone already inserted the page
    while (((1L<<loc64)&(old_val=__atomic_load_n(&n->is_inserting_page[locPage], __ATOMIC_ACQUIRE))) != (1L<<loc64))
    {  
        new_val = old_val | (1L<<loc64);
        success = __atomic_compare_exchange_n(&n->is_inserting_page[locPage], &old_val, new_val, /*strong*/0, __ATOMIC_ACQ_REL, __ATOMIC_ACQUIRE);
    }
    if (!success)
        return; // someone else is inserting
#ifndef NDEBUG
    off_t off_res =
#endif
    lseek(n->fd, location_in_file, SEEK_SET); // TODO: how does this work with PM?
    assert(-1 != off_res && "could not set position in file");
    int flags = MAP_FIXED;
    if (n->is_private)
        flags |= MAP_PRIVATE;
#ifndef NDEBUG
    void *res =
#endif
    mmap((void*)toMap, n->page_size, PROT_READ|PROT_WRITE, flags, n->fd, 0);
    assert((uintptr_t)res == toMap && "could not map the requested page");
    if (n->is_trigger_cow)
        for (uint64_t *a = (uint64_t *)toMap; a != (uint64_t *)(toMap+n->page_size); a += 4096 / sizeof(uint64_t))
            __atomic_compare_exchange(a, a, a,
                /*strong*/0, __ATOMIC_ACQ_REL, __ATOMIC_ACQUIRE);
    __atomic_or_fetch(&n->is_page_in[locPage], 1L<<loc64, __ATOMIC_RELEASE); // does not allow removals!
    __atomic_add_fetch(&curr_dram_buffer_size, n->page_size, __ATOMIC_RELEASE);
    __atomic_and_fetch(&n->is_inserting_page[locPage], ~(1L<<loc64), __ATOMIC_RELEASE); // does not allow removals!
}

static void heap_sigsegv_handler(int sig, siginfo_t *si, void *unused)
{
    uintptr_t addr = (uintptr_t)si->si_addr;
    range_top_node_s n = list_of_ranges;
    range_top_node_s where_to_remove;
    while (NULL != n)
    {
        if (addr - n->base_addr < n->size_range)
        {
            // inside range
            while (curr_dram_buffer_size > dram_buffer_size) // hard limit
                rm_page(where_to_remove, select_page_out(&where_to_remove));
            add_page(n, (void*)addr);
            return;
        }
        n = n->next;
    }
    // access is outside OUR heap! Thus, it is actually a SIGSEGV!
    printf("Got a true SIGSEGV at address: 0x%lx\n", addr);
    exit(EXIT_FAILURE);
}
