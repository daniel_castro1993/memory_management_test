#include "replayer_logs.h"

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/stat.h>

#include "util.h"

struct log_metadata
{
    volatile uint64_t start_ptr;
    volatile uint64_t end_ptr;
};

struct log_metadata_all
{
    struct log_metadata *thrd_ptrs;
    uint64_t log_size;
    uint64_t most_recent_ts;
    uint32_t nb_thrds;
    int32_t workers_done;
    uint8_t *padding[64-32];
};

struct repl_
{
    int max_thrs;
    volatile int count_thrs;
    uint64_t log_size;
    uint64_t log_mask;
    uint64_t tot_mmaped;
    uint64_t *earliest_ts_start;
    uint64_t *earliest_ts_per_log;
    struct log_metadata_all *metadata;
    void *base_addr_logs;
    uint64_t minimum_ts;
    int minimum_ts_thrd;
    int fd;
};

struct repl_thrd_
{
    uint64_t cur_ptr;
    uint64_t cpy_start_ptr;
    repl_s logs;
    uint64_t *my_log;
    int thrd_id;
};

struct repl_range_
{
    void *base;
    uint64_t range;
    int fd;
};

repl_s repl_init(
    const char *logs_file,
    int nb_thrds,
    uint64_t per_thrd_log_size
) {
    repl_s ret;
    int is_floor = 0;
    uint64_t log_mask = 0;
    uint64_t log2_log_size = fastlog2_and_mask(per_thrd_log_size, &log_mask, &is_floor);
    log2_log_size += is_floor;
    uint64_t size_one_log = 1L << log2_log_size;
    uint64_t tot_size_logs = size_one_log * nb_thrds;
    uint64_t size_per_thread_metadata = sizeof(struct log_metadata) * nb_thrds;
    uint64_t tot_size_metadata = size_per_thread_metadata + sizeof(struct log_metadata_all);
    uint64_t alloc_space_metadata_log2 = fastlog2_and_mask(tot_size_metadata, NULL, &is_floor);
    alloc_space_metadata_log2 += is_floor;
    uint64_t alloc_space_metadata = 1L << alloc_space_metadata_log2;
    uint64_t tot_size = tot_size_logs + alloc_space_metadata;
    struct stat logfile_buffer;
    
    ret = (repl_s)calloc(1, sizeof(struct repl_));
    assert(NULL != ret && "could not allocate logs");
    if (stat(logs_file, &logfile_buffer))
    {
        ret->fd = open(logs_file, O_CREAT | O_TRUNC | O_WRONLY, 0666);
        close(ret->fd); // writes the permissions
        if (-1 == (ret->fd = open(logs_file, O_RDWR, 0666)))
            fprintf(stderr, "Error open file \"%s\": %s\n", logs_file, strerror(errno));
        if (ftruncate(ret->fd, tot_size))
            fprintf(stderr, "Error ftruncate file \"%s\": %s\n", logs_file, strerror(errno));
    }
    else
    {
        if (-1 == (ret->fd = open(logs_file, O_RDWR, 0666)))
            fprintf(stderr, "Error open file \"%s\": %s\n", logs_file, strerror(errno));
        // file exists
        if (logfile_buffer.st_size < tot_size)
        {
            if (ftruncate(ret->fd, tot_size))
                fprintf(stderr, "Error ftruncate file \"%s\": %s\n", logs_file, strerror(errno));
        }
    }

    ret->tot_mmaped = tot_size;
    ret->metadata = (struct log_metadata_all*)mmap(NULL, tot_size, PROT_READ|PROT_WRITE, MAP_SHARED, ret->fd, 0);
    assert((struct log_metadata_all*)-1L != ret->metadata && "could not allocate log metadata");
    
    ret->metadata->thrd_ptrs = (struct log_metadata*)(((uintptr_t)ret->metadata) + sizeof(struct log_metadata_all));
    ret->base_addr_logs = (void*)(((uintptr_t)ret->metadata) + alloc_space_metadata);

    ret->metadata->log_size = size_one_log;
    ret->metadata->nb_thrds = nb_thrds;
    ret->log_size = size_one_log;
    ret->log_mask = log_mask;
    ret->max_thrs = nb_thrds;

    return ret;
}

int repl_thread_get_id(repl_thrd_s t)
{
    return t->thrd_id;
}

uint64_t repl_log_left(int log_id, repl_s r)
{
    return r->metadata->thrd_ptrs[log_id].end_ptr - r->metadata->thrd_ptrs[log_id].start_ptr;
}

void repl_logs_erase(repl_s r)
{
    memset(r->metadata->thrd_ptrs, 0, r->max_thrs*sizeof(struct log_metadata));
}

void repl_set_workers_done(repl_s r, int done)
{
    __atomic_store_n(&r->metadata->workers_done, done, __ATOMIC_RELEASE);
}

repl_thrd_s repl_thread_enter(repl_s r)
{
    repl_thrd_s ret;
    ret = (repl_thrd_s)calloc(1, sizeof(struct repl_thrd_));
    ret->logs = r;
    ret->thrd_id = __atomic_fetch_add(&r->count_thrs, 1, __ATOMIC_SEQ_CST);
    ret->my_log = (void*)(((uintptr_t)r->base_addr_logs) + r->log_size*ret->thrd_id);
    return ret;
}

void repl_thread_exit(repl_thrd_s l)
{
    free(l);
}

void repl_flush_mmap(repl_s r)
{
    msync(r->metadata, r->tot_mmaped, MS_SYNC); // TODO: not needed with PM
}

void repl_destroy(repl_s r)
{
    munmap(r->metadata, r->tot_mmaped);
    free(r);
}

// mmaps the range in shared mode
repl_range_s repl_mmap_range(
    const char *filename,
    void *base, 
    uint64_t range_size
) {
    repl_range_s ret;
    ret = (repl_range_s)calloc(1, sizeof(struct repl_range_));
    assert(NULL != ret && "could not allocate logs");
    ret->base = base;
    ret->range = range_size;
    ret->fd = open(filename, O_CREAT | O_TRUNC | O_RDWR, 0666);
    close(ret->fd); // writes the permissions
    if (-1 == (ret->fd = open(filename, O_CREAT | O_RDWR, 0666)))
        fprintf(stderr, "Error open file \"%s\": %s\n", filename, strerror(errno));
    if (ftruncate(ret->fd, range_size))
        fprintf(stderr, "Error ftruncate file \"%s\": %s\n", filename, strerror(errno));
#ifndef NDEBUG
    void *err =
#endif
    mmap(base, range_size, PROT_READ|PROT_WRITE, MAP_FIXED|MAP_SHARED, ret->fd, 0);
    assert(err == base && "could not mmap range");
    return ret;
}

void repl_munmap_range(repl_range_s r)
{
    munmap(r->base, r->range);
    close(r->fd);
    free(r);
}

// write a chunk with repl_store then end with repl_close
void repl_start(repl_thrd_s l)
{
    l->cpy_start_ptr = __atomic_load_n(
        &(l->logs->metadata->thrd_ptrs[l->thrd_id].start_ptr), __ATOMIC_ACQUIRE);
    l->cur_ptr = __atomic_load_n(
        &(l->logs->metadata->thrd_ptrs[l->thrd_id].end_ptr), __ATOMIC_ACQUIRE);
}

void repl_store(repl_thrd_s l, void *addr, uint64_t value)
{
    while (l->cpy_start_ptr == l->cur_ptr+1)
    {
        // wait update
        l->cpy_start_ptr = __atomic_load_n(
            &(l->logs->metadata->thrd_ptrs[l->thrd_id].start_ptr), __ATOMIC_ACQUIRE);
    }
    while (l->cpy_start_ptr == l->cur_ptr+2)
    {
        // wait update
        l->cpy_start_ptr = __atomic_load_n(
            &(l->logs->metadata->thrd_ptrs[l->thrd_id].start_ptr), __ATOMIC_ACQUIRE);
    }
    l->my_log[l->cur_ptr&l->logs->log_mask] = (uint64_t)addr;
    l->cur_ptr++;
    l->my_log[l->cur_ptr&l->logs->log_mask] = value;
    l->cur_ptr++;
}

void repl_close(repl_thrd_s l, uint64_t timestamp)
{
    uint64_t ts = (1L<<63)|timestamp;
    while (l->cpy_start_ptr == l->cur_ptr+1)
    {
        // wait update
        l->cpy_start_ptr = __atomic_load_n(
            &(l->logs->metadata->thrd_ptrs[l->thrd_id].start_ptr), __ATOMIC_ACQUIRE);
    }
    l->my_log[l->cur_ptr&l->logs->log_mask] = ts;
    l->cur_ptr++;
    __atomic_store_n(&(l->logs->metadata->thrd_ptrs[l->thrd_id].end_ptr), l->cur_ptr, __ATOMIC_RELEASE);
    uint64_t most_recent_ts;
    int success = 0;
    while (!success && (most_recent_ts = __atomic_load_n(&l->logs->metadata->most_recent_ts, __ATOMIC_ACQUIRE)) < timestamp)
    {
        success = __atomic_compare_exchange_n(&l->logs->metadata->most_recent_ts, &most_recent_ts, timestamp, /*strong*/0, __ATOMIC_ACQ_REL, __ATOMIC_ACQUIRE);
    }
}

void repl_replay_init(repl_s r)
{
    r->earliest_ts_per_log = (uint64_t*)malloc(r->max_thrs*sizeof(uint64_t));
    assert(NULL != r->earliest_ts_per_log && "could not allocate");
    memset(r->earliest_ts_per_log, -1L, r->max_thrs*sizeof(uint64_t));
    r->earliest_ts_start = (uint64_t*)malloc(r->max_thrs*sizeof(uint64_t));
    assert(NULL != r->earliest_ts_start && "could not allocate");
    memset(r->earliest_ts_start, -1L, r->max_thrs*sizeof(uint64_t));
    r->minimum_ts_thrd = -1;
}

static void setup_ts(repl_s r)
{
    for (int i = 0; i < r->max_thrs; ++i)
    {
        uint64_t *my_log = (uint64_t*)(((uintptr_t)r->base_addr_logs) + r->log_size*i);
        uint64_t start = __atomic_load_n(&r->metadata->thrd_ptrs[i].start_ptr, __ATOMIC_ACQUIRE);
        uint64_t log_ptr = start;
        uint64_t end = __atomic_load_n(&r->metadata->thrd_ptrs[i].end_ptr, __ATOMIC_ACQUIRE);
        uint64_t ts;
        if (log_ptr == end)
            continue; // empty log
        if (start == r->earliest_ts_start[i])
        {
            if ((-1 == r->minimum_ts_thrd && -1L != r->earliest_ts_per_log[i]) ||
                (-1 != r->minimum_ts_thrd && r->earliest_ts_per_log[i] < r->minimum_ts))
            {
                r->minimum_ts = r->earliest_ts_per_log[i];
                r->minimum_ts_thrd = i;
            }
            // printf("      nothing new in log %i (start=%lu, log_ptr=%lu, end=%lu)\n", i, start, log_ptr, end);
            continue;
        }
        while (log_ptr < end)
        {
            if ((ts = my_log[log_ptr&r->log_mask])&(1L<<63)) // found a timestamp
                break;
            log_ptr += 2;
        }
        assert(log_ptr <= end && "error start pointer is above the end pointer");
        // if (log_ptr > end)
        // {
        //     printf("log %i error!!!!!\n", i);
        // }
        ts &= ~(1L<<63);
        if (ts > __atomic_load_n(&r->metadata->most_recent_ts, __ATOMIC_ACQUIRE))
        {
            r->earliest_ts_start[i] = start;
            r->earliest_ts_per_log[i] = -1L;
            continue; // transaction is still not ready
        }
        // printf("found in log %i, ts=%lu\n", i, ts);
        if (0 == r->minimum_ts || r->minimum_ts > ts)
        {
            // printf("  r->minimum_ts_thrd = %i\n", i);
            r->minimum_ts = ts;
            r->minimum_ts_thrd = i;
        }
        // printf("    r->earliest_ts_start[%i]=%lu\n", i, start);
        r->earliest_ts_start[i] = start;
        r->earliest_ts_per_log[i] = ts;
    }
}

int repl_replay(repl_s r)
{
    setup_ts(r); // TODO: this is slow
    int thrd_id = r->minimum_ts_thrd;
    if (thrd_id == -1)
        goto end;
    uint64_t *my_log = (uint64_t*)(((uintptr_t)r->base_addr_logs) + r->log_size*thrd_id);
    uint64_t start = r->earliest_ts_start[thrd_id];
    uint64_t end = __atomic_load_n(&r->metadata->thrd_ptrs[thrd_id].end_ptr, __ATOMIC_ACQUIRE);
    uint64_t ts;
    while (start < end)
    {
        if ((ts = my_log[start&r->log_mask])&(1L<<63)) // found a timestamp
            break;
        uint64_t *addr = (uint64_t*)my_log[start&r->log_mask];
        start++;
        uint64_t value = (uint64_t)my_log[start&r->log_mask];
        start++;
        // printf("log %i writting %lu in addr %p\n", thrd_id, value, addr);
        *addr = value;
    }
    start++;
    r->earliest_ts_start[thrd_id] = start;
    __atomic_store_n(&r->metadata->thrd_ptrs[thrd_id].start_ptr, start, __ATOMIC_RELEASE);
end:
    r->minimum_ts_thrd = -1;
    return __atomic_load_n(&r->metadata->workers_done, __ATOMIC_ACQUIRE);
}

void repl_replay_exit(repl_s r)
{
    free(r->earliest_ts_start);
    free(r->earliest_ts_per_log);
}
