#ifndef UTIL_H_GUARD_
#define UTIL_H_GUARD_
// not very fast though...
static uint64_t fastlog2_and_mask(uint64_t n, uint64_t *mask, int *is_floor)
{
    uint64_t i = 0;
    uint64_t m = 0;
    int is_f = 0;
    while ( n > 1 )
    {
        n >>= 1L;
        m |= 1L << i;
        i++;
        if ((n & 1) && is_f == 0) // 1 bit is set
            is_f = 1;
        if ((n & 1) && is_f == 1) // 2 bits are set
            is_f = 2;
    }
    if (NULL != is_floor && is_f == 2)
        *is_floor = 1; // there is a rounding down
    if (mask)
        *mask = m;
    return i;
}
#endif /* UTIL_H_GUARD_ */