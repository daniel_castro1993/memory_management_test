CC     := gcc -c
LD     := gcc
AR     := ar rcv
CFLAGS := -g -O0 -std=gnu11 -Wall -I include/

SRCS   := \
	src/mmap_range.c \
	src/replayer_logs.c \
#
OBJS   := $(SRCS:.c=.o)
#
TESTS  := \
	tests/test_range_lib.c \
	tests/test_htm_abort_on_munmap.c \
	tests/main.c
#
TESTS_OBJS := $(TESTS:.c=.o)


main: tests/main.o
	$(LD) $(CFLAGS) $^ -o $@

test_log: test_log_workers test_log_background
	gdb --args ./test_log_workers

test_htm_abort_on_munmap: tests/test_htm_abort_on_munmap.o
	$(LD) $(CFLAGS) $^ -o $@ -mrtm -pthread

test_userfaultfd: tests/test_userfaultfd.o
	$(LD) $(CFLAGS) $^ -o $@ -pthread

test_range_lib: tests/test_range_lib.o src/mmap_range.o
	$(LD) $(CFLAGS) $^ -o $@ -pthread

test_log_workers: tests/test_log_workers.o src/mmap_range.o src/replayer_logs.o
	$(LD) $(CFLAGS) $^ -o $@ -pthread

test_log_background: tests/test_log_background.o src/mmap_range.o src/replayer_logs.o
	$(LD) $(CFLAGS) $^ -o $@ -pthread

tests/test_htm_abort_on_munmap.o: tests/test_htm_abort_on_munmap.c
	$(CC) $(CFLAGS) $^ -o $@ -mrtm

tests/test_range_lib.o: tests/test_range_lib.c
	$(CC) $(CFLAGS) $^ -o $@ -mrtm -I ./src -I ./include

%.o : %.c
	$(CC) $(CFLAGS) $^ -o $@

clean:
	rm -f $(OBJS) $(TESTS_OBJS) main \
		test_htm_abort_on_munmap test_userfaultfd test_range_lib test_log_workers test_log_background
