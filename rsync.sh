#!/bin/bash

DIR=projs
NAME="memory_management_test"
NODE="pascal"

DM=$DIR/$NAME

if [[ $# -gt 0 ]] ; then
	NODE=$1
fi

find . -name ".DS_Store" -delete
find . -name "._*" -delete

ssh $NODE "mkdir -p $DIR/$DM "
rsync -avzP . $NODE:$DM
