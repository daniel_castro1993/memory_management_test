
#include "mmap_range.h"
#include "replayer_logs.h"

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define DRAM_BUFFER_SIZE  268435456
#define HEAP_SIZE         2147483648
// #define HEAP_SIZE         34359738368
#define FILENAME          "./somefile.heap"
#define LOGS_FILENAME     "./somefile.logs"
// #define FILENAME          "/mnt/nvram0/somefile.heap"
// #define LOGS_FILENAME     "/mnt/nvram0/somefile.logs"
#define LOG_SIZE         2097152
#define HEAP_START_ADDR  0x0000001000000000L // TODO: the kernel may not allow (most likely it will)
#define PAGE_SIZE        2097152

#define NB_THREADS 10

int main()
{
    repl_range_s r = repl_mmap_range(
        FILENAME, (void*)HEAP_START_ADDR, HEAP_SIZE);
    repl_s l = repl_init(LOGS_FILENAME, NB_THREADS, LOG_SIZE);
    repl_replay_init(l);
    
    while(!repl_replay(l))
    {
        // repl_flush_mmap(l); // TODO: not needed for PM
    }

    repl_set_workers_done(l, 0);
    repl_destroy(l);
    repl_munmap_range(r);
    return EXIT_SUCCESS;
}


