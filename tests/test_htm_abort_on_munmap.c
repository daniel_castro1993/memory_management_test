#include <stdio.h>
#include <stdlib.h>

#include <errno.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>
#include <sys/mman.h>
#include <pthread.h>

// TSX
#include <immintrin.h> // includes avx512 now
#define _IMMINTRIN_H_INCLUDED
#include <xtestintrin.h>
#include <rtmintrin.h>

#include "rdtsc.h"

#define MEMORY_HEAP_FILE "./somefile.heap"
// #define MEMORY_HEAP_SIZE 34359738368 // the entire heap is 32GB, but...
#define MEMORY_HEAP_SIZE 1073741824 // the entire heap is 1GB, but...
// #define MEMORY_HEAP_SIZE 16777216 // the entire heap is 16MB, but...
#define MEMORY_HEAP_MMAP 1048576  // maximum mapped in DRAM is 1MB
// #define MEMORY_HEAP_MMAP 262144  // maximum mapped in DRAM is 256kB

// https://unix.stackexchange.com/questions/509607/how-a-64-bit-process-virtual-address-space-is-divided-in-linux
#define HEAP_START_ADDR  0x0000001000000000L // TODO: the kernel may not allow (most likely it will)
#define PAGE_SIZE        4096
#define PAGE_SIZE_BITS   12
#define PAGE_MASK        0b111111111111L // ==0xFFFL (12 bits, be sure you have the L in the end)
// #define NB_PAGES         8388608 // 8388608 pages of size 4096B == 32GB
// #define NB_PAGES_64BITS  131072  // each 64 bit bitmap chunk maps 64 pages (131072*64==8388608 pages)
#define NB_PAGES         262144 // 262144 pages of size 4096B == 1GB
#define NB_PAGES_64BITS  4096   // each 64 bit bitmap chunk maps 64 pages (4096*64==262144 pages)
// #define NB_PAGES         4096 // 4096 pages of size 4096B == 16MB
// #define NB_PAGES_64BITS  64   // each 64 bit bitmap chunk maps 64 pages (64*64==4096 pages)

// only used by the signal handler
// TODO: in multithread env we need atomics!
static volatile uint64_t pages[NB_PAGES_64BITS] = {0};
static volatile int curr_size_of_heap = 0;
static volatile int heap_fd = -1;

static void add_page(void *addr)
{
    uintptr_t toMap = (uintptr_t)addr;
    toMap &= ~PAGE_MASK;
    uintptr_t a = toMap;
    a -= HEAP_START_ADDR; // subtracts base addr
    off_t location_in_file = (off_t)a;
    // printf("add_page toMap = %lx, a = %lx (MEMORY_HEAP_SIZE = %lx)\n", toMap, a, MEMORY_HEAP_SIZE);
    assert(a < MEMORY_HEAP_SIZE);
    a >>= PAGE_SIZE_BITS;
    int loc64    = a & 0x3F;
    int locPage  = a >> 6; // log2(64) == 6
    // printf("toMap = %lx, a = %lx (loc64 = %i, locPage = %i)\n", toMap, a, loc64, locPage);
    assert(!((1L<<loc64) == (pages[locPage] & (1L<<loc64))) && "bit was already set!");
    pages[locPage] |= (1L<<loc64);
    // printf("after add_page = %lx (locPage = %i, addr = %p)\n", pages[locPage], locPage, addr);
#ifndef NDEBUG
    off_t off_res =
#endif
    lseek(heap_fd, location_in_file, SEEK_SET); // TODO: how does this work with PM?
    assert(-1 != off_res && "could not set position in file");
#ifndef NDEBUG
    void *res =
#endif
    mmap((void*)toMap, PAGE_SIZE, PROT_READ|PROT_WRITE, MAP_PRIVATE|MAP_FIXED, heap_fd, 0);
    curr_size_of_heap += PAGE_SIZE;
    // perror("mmap()");
    assert((uintptr_t)res == toMap && "could not map the requested page");
    // int wasPageIn = (1<<loc64) == (pages[locPage] & (1<<loc64)); // TODO: the == can be avoided
    // return wasPageIn;
}

static void rm_page(void *addr)
{
    uintptr_t toUnmap = (uintptr_t)addr;
    toUnmap &= ~PAGE_MASK;
    uintptr_t a = toUnmap;
    a -= HEAP_START_ADDR;
    // if (!(a < MEMORY_HEAP_SIZE))
    //     printf("before rm_page %p toUnmap = %lx a = %lx HEAP_START_ADDR = %p\n", addr, toUnmap, a, (void*)HEAP_START_ADDR);
    assert(a < MEMORY_HEAP_SIZE);
    a >>= PAGE_SIZE_BITS;
    int loc64    = a & 0x3F;
    int locPage  = a >> 6;
    // printf("before rm_page %p (bitmap = %lx, loc64 = %i)\n", addr, pages[locPage], loc64);
    assert((1L<<loc64) == (pages[locPage] & (1L<<loc64)) && "bit was not set!");
    pages[locPage] &= ~(1L<<loc64);
    // printf("after rm_page %p (bitmap = %lx, mask = %lx)\n", addr, pages[locPage], ~(1L<<loc64));
#ifndef NDEBUG
    int err =
#endif
    munmap((void*)toUnmap, PAGE_SIZE);
    curr_size_of_heap -= PAGE_SIZE;
    assert(!err && "could not unmap the page");
}

static int rightmostbit(uint64_t b)
{
    for (int i = 0; i < 64; ++i)
    {
        if (b & (1L<<i))
        {
            return i;
        }
    }
    return -1;
}

static void *select_page_out() // just throws away the first page it finds (try random or some statistic)
{
    uintptr_t i; // TODO: be extremely careful with 64 bit math (by default in linux int and constants are 32 bits)
    for (i = 0; i < NB_PAGES_64BITS; ++i)
    {
        if (0 != pages[i])
        {
            uintptr_t rmb = rightmostbit(pages[i]); // TODO: throw some access statistic here
            uintptr_t offset = 64L*i + rmb;
            uintptr_t pagetorm = HEAP_START_ADDR + offset*PAGE_SIZE;
            // printf("select_page_out rmb = %i page = %i, addr = %p\n", rmb, offset, (void*)pagetorm);
            return (void*)pagetorm;
        }
    }
    assert(0 && "there are not pages to remove!");
    return NULL;
}

static void prepare_heap()
{
    heap_fd = open(MEMORY_HEAP_FILE, O_CREAT | O_TRUNC | O_RDWR, 0666);
    close(heap_fd); // writes the permissions
    if (-1 == (heap_fd = open(MEMORY_HEAP_FILE, O_CREAT | O_RDWR, 0666)))
        fprintf(stderr, "Error open file \"%s\": %s\n", MEMORY_HEAP_FILE, strerror(errno));
    if (ftruncate(heap_fd, MEMORY_HEAP_SIZE))
        fprintf(stderr, "Error ftruncate file \"%s\": %s\n", MEMORY_HEAP_FILE, strerror(errno));
    // note, nothing is allocated here
}

static void heap_sigsegv_handler(int sig, siginfo_t *si, void *unused)
{
    uintptr_t addr = (uintptr_t)si->si_addr;
    if (addr - HEAP_START_ADDR >= MEMORY_HEAP_SIZE)
    {
        // access is outside OUR heap! Thus, it is actually a SIGSEGV!
        printf("Got a true SIGSEGV at address: 0x%lx\n", addr);
        exit(EXIT_FAILURE);
    }
    if (curr_size_of_heap > MEMORY_HEAP_MMAP)
        rm_page(select_page_out());
    add_page((void*)addr);
}

#define SAMPLES 100

static volatile int communication = 0;

static void *tsx_thread(void *args)
{
    uint64_t * volatile addr = (uint64_t*)HEAP_START_ADDR;
    __atomic_store_n(addr   , 1, __ATOMIC_RELEASE); // populates 4 cache lines
    __atomic_store_n(addr+8 , 2, __ATOMIC_RELEASE);
    __atomic_store_n(addr+16, 3, __ATOMIC_RELEASE);
    __atomic_store_n(addr+32, 4, __ATOMIC_RELEASE);

    for (volatile int j = 0; j < SAMPLES; ++j)
    {
        int status;
        __atomic_store_n(addr, 1, __ATOMIC_RELEASE); // makes sure the page is mapped and CoWed
        for (volatile int i = 0; i < 1000; ++i); // wait a bit
        if (_XBEGIN_STARTED != (status = _xbegin()))
        {
            // is abort!
            __atomic_store_n(addr, 1, __ATOMIC_RELEASE);
            printf("[%i] TSX aborted with error code %i\n", j, status);
        }
        else
        {
            *addr = 123456;
            for (volatile int i = 0; i < 50000; ++i);
            _xend();
            __atomic_store_n(addr, 1, __ATOMIC_RELEASE);
            printf("[%i] TSX committed\n", j);
        }
        __atomic_store_n(&communication, j, __ATOMIC_RELEASE);
    }
    return NULL;
}

int main()
{
    struct sigaction sa;
    uint64_t *addr = (uint64_t*)HEAP_START_ADDR;

    prepare_heap(); // does not allocate anything

    sa.sa_flags = SA_SIGINFO;
    sigemptyset(&sa.sa_mask);
    sa.sa_sigaction = heap_sigsegv_handler;
    if (-1 == sigaction(SIGSEGV, &sa, NULL))
        perror("sigaction()");


    // TODO: if both try to map concurrently it will probably not work
    __atomic_store_n(addr+32, 0, __ATOMIC_RELEASE);

    pthread_t tsx_thrd;
    if (pthread_create(&tsx_thrd, NULL, tsx_thread, NULL))
        perror("pthread_create()");

    // the memory accesses will lazily map the pages and trigger the CoW
    while (4 != __atomic_load_n(addr+32, __ATOMIC_ACQUIRE)); // wait tsx_starting

    for (volatile int j = 0; j < SAMPLES; ++j)
    {
        for (volatile int i = 0; i < 10000; ++i);
        rm_page(addr); // munmaps the page that TSX is accessing (causing abort)
        // for (volatile int i = 0; i < 5000; ++i);
        printf("[%i]   - munmapped page!\n", j);
        while (j != __atomic_load_n(&communication, __ATOMIC_ACQUIRE)); // wait tsx commit/abort
    }

    return EXIT_SUCCESS;
}

