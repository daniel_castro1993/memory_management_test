
#include "mmap_range.h"
#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define DRAM_BUFFER_SIZE  268435456
#define HEAP_SIZE         2147483648
// #define HEAP_SIZE         34359738368
#define FILENAME          "./somefile.heap"
#define HEAP_START_ADDR  0x0000001000000000L // TODO: the kernel may not allow (most likely it will)
#define PAGE_SIZE        2097152

#define NB_THREADS 50

void *thread_fn(void *arg)
{
    mmap_range_thread_enter();
    uint64_t *heap = (uint64_t*)HEAP_START_ADDR;
    uint64_t *heap_end = heap + (HEAP_SIZE/sizeof(uint64_t));
    int id = *(int*)arg;

    for (int i = 0; i < (NB_THREADS - id)+2; ++i)
    {
        for (uint64_t *j = heap; j < heap_end; j += PAGE_SIZE/((id+1)*sizeof(uint64_t)))
        {
            // printf("[%i] write on %p\n", id, j);
            *j = 123456;
        }
    }
    mmap_range_thread_exit();
    printf("Thread %i is done\n", id);
    return NULL;
}

static void swap_in(void *addr, void *metadata, void *args)
{
    __atomic_add_fetch((uint64_t*)args, 1, __ATOMIC_RELEASE);
    // TODO: redo logs
}

static int swap_out(void **addr, void *metadata, void *args)
{
    uint64_t nb_pages_in = __atomic_fetch_sub((uint64_t*)args, 1, __ATOMIC_RELEASE);
    // TODO: hint the page in *addr
    return nb_pages_in > 600; // while there is more than 100 pages swap-out
}

int main()
{
    int thrs_ids[NB_THREADS];
    uint64_t nb_pages_in = 0;
    pthread_t thrs[NB_THREADS-1];
    mmap_range_init(DRAM_BUFFER_SIZE);
    mmap_range_add_range(
        FILENAME,
        (void*)HEAP_START_ADDR,
        HEAP_SIZE,
        PAGE_SIZE,
        sizeof(uint64_t),
        /*is_private*/  1,
        /*trigger_cow*/ 1,
        swap_in,
        swap_out,
        &nb_pages_in
    );
    
    for (int i = 0; i < NB_THREADS-1; ++i)
    {
        thrs_ids[i] = i;
        if (pthread_create(&thrs[i], NULL, thread_fn, &thrs_ids[i]))
        {
            perror("pthread_create()");
            exit(EXIT_FAILURE);
        }
    }
    thrs_ids[NB_THREADS-1] = NB_THREADS-1;
    thread_fn((void*)&thrs_ids[NB_THREADS-1]);

    for (int i = 0; i < NB_THREADS-1; ++i)
        pthread_join(thrs[i], NULL);

    mmap_range_destroy();

    return EXIT_SUCCESS;
}


