
#include "mmap_range.h"
#include "replayer_logs.h"
#include "rdtsc.h"

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>
#include <spawn.h>
#include <sys/wait.h>

#define DRAM_BUFFER_SIZE  268435456
#define HEAP_SIZE         2147483648
// #define HEAP_SIZE         34359738368
#define FILENAME          "./somefile.heap"
#define LOGS_FILENAME     "./somefile.logs"
// #define FILENAME          "/mnt/nvram0/somefile.heap"
// #define LOGS_FILENAME     "/mnt/nvram0/somefile.logs"
#define LOG_SIZE         2097152
#define HEAP_START_ADDR  0x0000001000000000L // TODO: the kernel may not allow (most likely it will)
#define PAGE_SIZE        2097152

#define NB_THREADS 10

void *thread_fn(void *arg)
{
    repl_s l = (repl_s)arg;
    range_thread_enter();
    repl_thrd_s t = repl_thread_enter(l);
    uint64_t *heap = (uint64_t*)HEAP_START_ADDR;
    uint64_t *heap_end = heap + (HEAP_SIZE/sizeof(uint64_t));
    int id = range_thread_get_id();

    for (int i = 0; i < (NB_THREADS - id)+2; ++i)
    {
        for (uint64_t *j = heap; j < heap_end; j += PAGE_SIZE/((id+1)*sizeof(uint64_t)))
        {
            // printf("[%i] write on %p\n", id, j);
            repl_start(t);
            *j = 123456;
            repl_store(t, j, 123456);
            uint64_t ts = rdtscp();
            repl_close(t, ts);
            // printf("close write %lx\n", ts);
        }
        // repl_flush_mmap(l); // TODO: not needed for PM
    }
    range_thread_exit();
    repl_thread_exit(t);
    printf("Thread %i is done\n", id);
    return NULL;
}

static void swap_in(void *addr, void *metadata, void *args)
{
    __atomic_add_fetch((uint64_t*)args, 1, __ATOMIC_RELEASE);
    // TODO: redo logs
}

static int swap_out(void **addr, void *metadata, void *args)
{
    uint64_t nb_pages_in = __atomic_fetch_sub((uint64_t*)args, 1, __ATOMIC_RELEASE);
    // TODO: hint the page in *addr
    return nb_pages_in > 600; // while there is more than 100 pages swap-out
}

extern char **environ;

int main()
{
    uint64_t nb_pages_in = 0;
    pthread_t thrs[NB_THREADS-1];
    repl_s r;
    char *argv_replayer[] = { NULL };
    range_init(DRAM_BUFFER_SIZE);
    range_add(
        FILENAME,
        (void*)HEAP_START_ADDR,
        HEAP_SIZE,
        PAGE_SIZE,
        sizeof(uint64_t),
        /*is_private*/  1,
        /*trigger_cow*/ 1,
        swap_in,
        swap_out,
        &nb_pages_in
    );
    
    r = repl_init(LOGS_FILENAME, NB_THREADS, LOG_SIZE);
    repl_logs_erase(r);
    repl_set_workers_done(r, 0);

    int status;
    pid_t replayer_pid;
    if (posix_spawn(&replayer_pid, "./test_log_background", NULL, NULL, argv_replayer, environ))
        perror("posix_spawn()");

    for (int i = 0; i < NB_THREADS-1; ++i)
    {
        if (pthread_create(&thrs[i], NULL, thread_fn, (void*)r))
        {
            perror("pthread_create()");
            exit(EXIT_FAILURE);
        }
    }
    thread_fn((void*)r);

    for (int i = 0; i < NB_THREADS-1; ++i)
        pthread_join(thrs[i], NULL);

    for (int i = 0; i < NB_THREADS; ++i)
        printf("[%i] log left = %lu\n", i, repl_log_left(i, r));

    range_destroy();
    // repl_flush_mmap(r); // TODO: not needed for PM

    sleep(1);
    repl_set_workers_done(r, 1);

    do {
        if (waitpid(replayer_pid, &status, 0) != -1) {
            printf("Child status %d\n", WEXITSTATUS(status));
        } else {
            perror("waitpid");
            exit(EXIT_FAILURE);
        }
    } while (!WIFEXITED(status) && !WIFSIGNALED(status));

    for (int i = 0; i < NB_THREADS; ++i)
        printf("[%i] log left = %lu\n", i, repl_log_left(i, r));

    repl_destroy(r);

    return EXIT_SUCCESS;
}


